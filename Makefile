# Makefile for LaTeX documents. Modified from
# <https://tex.stackexchange.com/a/40759>


# VARIABLES
# SET THIS ONE to the file name of the main TeX file
MAIN_TEX_FILE := thesis.tex

# Tools
LATEXMK := latexmk
LATEXMK_FLAGS := -pdf -use-make \
                 -pdflatex="pdflatex -interaction=nonstopmode -synctex=1"

# Remove possible .tex extension from main file name
MAIN_TEX_BASENAME := $(basename $(MAIN_TEX_FILE))


# Include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: all clean watch FORCE


# Delete files when their creation failed. Prevents broken target files.
.DELETE_ON_ERROR:


# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
all: $(MAIN_TEX_BASENAME).pdf


# CUSTOM BUILD RULES

# In case you didn't know, '$@' is a variable holding the name of the target,
# and '$<' is a variable holding the (first) prerequisite of a rule.
# "raw2tex" and "dat2tex" are just placeholders for whatever custom steps
# you might have.

# %.tex: %.raw
# 	./raw2tex $< > $@
#
# %.tex: %.dat
# 	./dat2tex $< > $@

# MAIN LATEXMK RULE
# -pdf tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make tells latexmk to call make for generating missing files.
# -interaction=nonstopmode keeps the pdflatex backend from stopping at a
# missing file reference and interactively asking you for an alternative.
$(MAIN_TEX_BASENAME).pdf: $(MAIN_TEX_BASENAME).tex FORCE
	$(LATEXMK) $(LATEXMK_FLAGS) $<


# Empty recipe. Recipes for targets specifying FORCE as prerequisite will
# always be executed. Similar to .PHONY targets, BUT .PHONY means that the
# target name is NOT a file which is not correct for the main TeX file. The
# FORCE target itself, however, should be PHONY to still work if there is a
# file called FORCE.
FORCE:


# Remove all generated TeX files
clean:
	$(LATEXMK) -CA
	rm -f $(MAIN_TEX_BASENAME){-blx.bib,.bbl,.run.xml}    # biblatex files


MAKE_CMD = $(MAKE) all && echo 'Success.' || echo 'ERROR, see above.'

watch:
	@[ -n "$$(command -v inotifywait)" ] || \
	    { echo 'inotifywait required but not found'; exit 1; }
	-$(MAKE_CMD)            # dash means do not die on compilation errors
	@while true; do \
	    echo -n 'Monitoring files... '; \
	    FILE=$$(inotifywait -qre close_write . | sed -n 's/.*\s\(\w\)/\1/p'); \
	    echo "$$FILE"; \
	    sleep 0.2; \
	    $(MAKE_CMD) ; \
	done

